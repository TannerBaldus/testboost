__author__ = 'tanner'
import  os
from settings import INSTALLED_FRAMEWORKS
from parse_tools import print_test_names

nodes = INSTALLED_FRAMEWORKS['nunit']['nodes']
print_test_names('tests/test_data/nunit-framework-master/src/tests',True,'*.cs',nodes, True)
