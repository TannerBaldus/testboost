import re


##################################################
# Helper Functions for Building Framework Parsers
##################################################
def c_class(text):
    """
    Isolates class name for c_lang family.
    :param text: text to be searched
    :return: class name if there is a match, None Otherwise
    """
    if re.search(r'class', text):
        return  re.sub(r'[-(<: ].*', '', re.sub(r'.*class(. ?)', '', text))
    return None


replace = lambda text, substitute: substitute if substitute != text else None

def match_string(pattern, text):
    match = re.search(pattern, text)
    if match:
        return match.string
    return None



##################################################
# Framework Parser Definitions
##################################################
nunit = {'command': '/run={suite} /nologo {test_file}',
         'strip': True,
         'nodes': [
         {'depth': 0, 'initial': lambda text: replace(text, re.sub(r'namespace(. ?)', '', text))},
         {'depth': 1, 'initial': lambda text: re.search('\[TestFixture.*\]', text), 'after': lambda text: c_class(text)},
                ]
         }





#####################################################
# Add Framework Name: Framework Definition Pairs Here
#####################################################
INSTALLED_FRAMEWORKS = {'nunit': nunit}


MAKEFILE_TEMPLATE = """
# TESTRUNNER:       the path to the testrunner
# LIST_SUITES:      how to list available test suites.
# RUN_ONE_SUITE:    how to run a single suite, should use the special
#           variable $1 which will be replaced by the suite name.

TESTRUNNER    = ./{testrunner}
LIST_SUITES   = {list_cmd}
RUN_ONE_SUITE = $(TESTRUNNER) {run_cmd}

###############################################################################
# DO NOT MODIFY BELOW THIS LINE

include suites.def
TARGETS=$(addsuffix _test,$(SUITES))

all: $(TARGETS)

%_test:
	$(call RUN_ONE_SUITE,$*)

suites.def: $(TESTRUNNER)
	echo "SUITES=" > $@
	for n in `$(LIST_SUITES)` ; do echo "SUITES += $$n" >> $@ ; done
"""